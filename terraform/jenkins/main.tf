provider "aws" {
    region = "us-east-1"
    access_key = ""
    secret_key = ""
}

variable "name" {
    description = "Instance name for deploy"
}

resource "aws_instance" "jenkins" {
    ami = "ami-0aeeebd8d2ab47354"
    instance_type = "t2.micro"
    key_name = "key3"

    tags = {
        Name = "${var.name}"
    }
} 