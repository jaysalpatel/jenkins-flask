provider "aws" {
    region = "us-east-1"
    access_key = ""
    secret_key = ""
}

variable "name" {
    description = "Instance name for flask instance"
}

resource "aws_instance" "web" {
    ami = "ami-0aeeebd8d2ab47354"
    instance_type = "t2.micro"
    key_name = "key3"

    tags = {
        Name = "${var.name}"
    }
}